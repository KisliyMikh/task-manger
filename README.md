# Task manager  

## Требования  

* JRE 8  

## Используемый стек

* Java SE 8
* Maven 3.6  

## Обратная свзяь
mail : *[kmsperm97@mail.ru]()* 

## Сбор и запуск приложения  

### Команды для сборки приложения  

* mvn clean   *#очистить проект*
* mvn [install]()   *#установить проект*

### Команды для запуска приложения  

* java -jar task-manger-1.0-SNAPSHOT.jar

