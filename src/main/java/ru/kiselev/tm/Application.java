package ru.kiselev.tm;


public class Application {
    private static final Bootstrap bootstrap = new Bootstrap();


    public static void main(String[] args) {
        bootstrap.run();
    }


}
