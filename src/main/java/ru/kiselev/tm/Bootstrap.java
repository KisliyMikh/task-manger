package ru.kiselev.tm;

import lombok.SneakyThrows;
import ru.kiselev.tm.commands.CommandsExecutor;
import ru.kiselev.tm.commands.CommandsHolder;
import ru.kiselev.tm.commands.CommandsInterface;
import ru.kiselev.tm.commands.other.Exit;
import ru.kiselev.tm.commands.other.Help;
import ru.kiselev.tm.commands.project.*;
import ru.kiselev.tm.constants.Constants;
import ru.kiselev.tm.repository.ProjectRepository;
import ru.kiselev.tm.repository.TaskRepository;
import ru.kiselev.tm.service.Terminal;

import java.util.HashMap;
import java.util.Map;

public class Bootstrap implements Runnable {
    private final Terminal terminal = new Terminal();
    private final ProjectRepository projectRepository = new ProjectRepository();
    private final TaskRepository taskRepository = new TaskRepository();
    private final Map<String, CommandsInterface> commands = new HashMap<>() {{
        put(Constants.CREATE_PROJECT, new CreateProject(projectRepository, terminal));
        put(Constants.DELETE_PROJECT, new DeleteProject(projectRepository, taskRepository, terminal));
        put(Constants.EDIT_PROJECT, new EditProject(projectRepository, taskRepository, terminal));
        put(Constants.VIEW_ALL_PROJECTS, new ViewAllProjects(projectRepository, terminal));
        put(Constants.VIEW_PROJECT, new ViewProject(projectRepository, taskRepository, terminal));
        put(Constants.HELP, new Help(this, terminal));
        put(Constants.EXIT, new Exit());
    }};
    private final CommandsHolder commandsHolder = new CommandsHolder(commands);
    private final CommandsExecutor commandsExecutor = new CommandsExecutor(commandsHolder, terminal);

    @SneakyThrows
    @Override
    public void run() {
        terminal.println("*** Добро пожаловать в Task Manager ***\n");
        terminal.println("Введите одну из ниже предложенных команд:\n");
        for (CommandsInterface command : commands.values()) {
            terminal.println(command.getName() + " - " + command.getDescription());
        }

        while (true) {
            terminal.println("Введите комманду:");
            String command = terminal.readLine();
            if ("exit".equalsIgnoreCase(command)) {
                return;
            }
            commandsExecutor.execute(command);
        }
    }
}
