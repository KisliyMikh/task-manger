package ru.kiselev.tm.commands;

import lombok.Data;
import ru.kiselev.tm.service.Terminal;

@Data
public class CommandsExecutor {
    private final CommandsHolder commandsHolder;
    private final Terminal terminal;

    public void execute(String command) {
        try {
            CommandsInterface commandsInterface = commandsHolder.getCommand(command.trim());
            commandsInterface.execute();
        } catch (Exception e) {
            terminal.println("Ошибка во время выполнения: " + e.getMessage());
        }
    }

}
