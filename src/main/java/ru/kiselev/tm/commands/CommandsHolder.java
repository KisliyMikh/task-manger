package ru.kiselev.tm.commands;

import ru.kiselev.tm.exception.CommandNotFoundException;

import java.util.Map;

public class CommandsHolder {
    private final Map<String, CommandsInterface> commands;

    public CommandsHolder(Map<String, CommandsInterface> commands) {
        this.commands = commands;
    }

    public CommandsInterface getCommand(String commandName) {
        CommandsInterface command = commands.get(commandName);
        if (command == null) {
            throw new CommandNotFoundException(commandName);
        }
        return command;
    }
}