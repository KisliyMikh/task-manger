package ru.kiselev.tm.commands;

import java.io.IOException;

public interface CommandsInterface {

    String getName();

    String getDescription();

    void execute() throws IOException;
}
