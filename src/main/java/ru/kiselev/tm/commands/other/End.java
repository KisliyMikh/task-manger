package ru.kiselev.tm.commands.other;

import lombok.Data;
import ru.kiselev.tm.commands.CommandsInterface;
import ru.kiselev.tm.constants.Constants;


@Data

public class End implements CommandsInterface {
    private final String name = Constants.END;
    private final String description = "завершить редактирование.";

    @Override
    public void execute() {
    }
}
