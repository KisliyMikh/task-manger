package ru.kiselev.tm.commands.other;

import lombok.Data;
import ru.kiselev.tm.commands.CommandsInterface;
import ru.kiselev.tm.constants.Constants;


@Data

public class Exit implements CommandsInterface {
    private final String name = Constants.EXIT;
    private final String description = "завершить работу программы.";

    @Override
    public void execute() {
    }
}
