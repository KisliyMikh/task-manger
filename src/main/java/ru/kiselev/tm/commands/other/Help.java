package ru.kiselev.tm.commands.other;

import lombok.Data;
import ru.kiselev.tm.commands.CommandsInterface;
import ru.kiselev.tm.constants.Constants;
import ru.kiselev.tm.service.Terminal;

import java.util.Map;


@Data

public class Help implements CommandsInterface {
    private final String name = Constants.HELP;
    private final String description = "список доступных комманд.";

    private final Map<String, CommandsInterface> commands;
    private final Terminal terminal;

    @Override
    public void execute() {
        for (CommandsInterface command : commands.values()) {
            terminal.println(command.getName() + " - " + command.getDescription());
        }
    }
}
