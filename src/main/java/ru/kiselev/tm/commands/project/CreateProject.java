package ru.kiselev.tm.commands.project;

import lombok.Data;
import ru.kiselev.tm.commands.CommandsInterface;
import ru.kiselev.tm.constants.Constants;
import ru.kiselev.tm.entity.Project;
import ru.kiselev.tm.repository.ProjectRepository;
import ru.kiselev.tm.service.StringUtils;
import ru.kiselev.tm.service.Terminal;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;

@Data

public class CreateProject implements CommandsInterface {
    private final String name = Constants.CREATE_PROJECT;
    private final String description = "создать проект.";

    private final ProjectRepository projectRepository;
    private final Terminal terminal;

    @Override
    public void execute() throws IOException {
        Project project = new Project();
        terminal.println("Введите название проекта:");
        String name = terminal.readLine();
        boolean projectName = StringUtils.isEmpty(name);
        if (projectName == true) {
            throw new IllegalArgumentException("Не введено название проекта! Повторите попытку.");
        } else {
            project.setNameProject(name);
        }
        terminal.println("Введите описание проекта:");
        String description = terminal.readLine();
        project.setDescription(description);
        terminal.println("Введите дату начала проекта в формате ГГГГ-ММ-ДД:");
        setDateProjectStart(project);
        terminal.println("Введите дату окончания проекта в формате ГГГГ-ММ-ДД:");
        setDateProjectEnd(project);
        projectRepository.create(project);
        terminal.println("Проект успешно создан! ID проекта:" + project.getId());
    }

    protected void setDateProjectStart(Project project) throws IOException {
        String dateStart = terminal.readLine();
        if (dateStart.isEmpty()) {
            project.setDataStart(null);
        } else {
            try {
                LocalDate localDateProject = LocalDate.parse(dateStart);
                project.setDataStart(localDateProject);
            } catch (DateTimeParseException e) {
                terminal.println("Неверно введена дата! Повторите попытку.");
                setDateProjectStart(project);
            }
        }
    }

    protected void setDateProjectEnd(Project project) throws IOException {
        String dateEnd = terminal.readLine();
        if (dateEnd.isEmpty()) {
            project.setDataEnd(null);
        } else {
            try {
                LocalDate localDateProject = LocalDate.parse(dateEnd);
                project.setDataEnd(localDateProject);
            } catch (DateTimeParseException e) {
                terminal.println("Неверно введена дата! Повторите попытку.");
                setDateProjectEnd(project);
            }
        }
    }
}
