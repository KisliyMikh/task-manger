package ru.kiselev.tm.commands.project;

import lombok.Data;
import ru.kiselev.tm.commands.CommandsExecutor;
import ru.kiselev.tm.commands.CommandsHolder;
import ru.kiselev.tm.commands.CommandsInterface;
import ru.kiselev.tm.commands.other.End;
import ru.kiselev.tm.commands.other.Help;
import ru.kiselev.tm.commands.project.update.UpdateDateEndProject;
import ru.kiselev.tm.commands.project.update.UpdateDateStartProject;
import ru.kiselev.tm.commands.project.update.UpdateDescriptionProject;
import ru.kiselev.tm.commands.project.update.UpdateNameProject;
import ru.kiselev.tm.commands.task.CreateTask;
import ru.kiselev.tm.commands.task.EditTask;
import ru.kiselev.tm.commands.task.ViewAllTasks;
import ru.kiselev.tm.commands.task.ViewTask;
import ru.kiselev.tm.constants.Constants;
import ru.kiselev.tm.entity.Project;
import ru.kiselev.tm.repository.ProjectRepository;
import ru.kiselev.tm.repository.TaskRepository;
import ru.kiselev.tm.service.EditorContext;
import ru.kiselev.tm.service.Terminal;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Data
public class EditProject implements CommandsInterface {
    private final String name = Constants.EDIT_PROJECT;
    private final String description = "редактировать проект.";

    private final ProjectRepository projectRepository;
    private final TaskRepository taskRepository;
    private final Terminal terminal;
    private Map<String, CommandsInterface> projectCommands;
    private final CommandsHolder commandsHolder;
    private final CommandsExecutor commandsExecutor;

    public EditProject(ProjectRepository projectRepository, TaskRepository taskRepository, Terminal terminal) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
        this.terminal = terminal;
        this.projectCommands = new HashMap<>() {{
            put(Constants.UPDATE_NAME, new UpdateNameProject(terminal));
            put(Constants.UPDATE_DESCRIPTION, new UpdateDescriptionProject(terminal));
            put(Constants.UPDATE_DATE_START, new UpdateDateStartProject(terminal));
            put(Constants.UPDATE_DATE_END, new UpdateDateEndProject(terminal));
            put(Constants.CREATE_TASK, new CreateTask(terminal, taskRepository));
            put(Constants.VIEW_TASK, new ViewTask(terminal, taskRepository));
            put(Constants.VIEW_ALL_TASKS, new ViewAllTasks(taskRepository, terminal));
            put(Constants.EDIT_TASK, new EditTask(taskRepository, projectRepository, terminal));
            put(Constants.HELP, new Help(this, terminal));
            put(Constants.END, new End());
        }};
        this.commandsHolder = new CommandsHolder(projectCommands);
        this.commandsExecutor = new CommandsExecutor(commandsHolder, terminal);
    }


    @Override
    public void execute() throws IOException {
        terminal.println("Введите ID проекта:");
        Long idProject = Long.parseLong(terminal.readLine());
        Project project = projectRepository.getProject(idProject);
        terminal.println("Введите одну из ниже предложенных команд:\n");
        for (CommandsInterface commands : projectCommands.values()) {
            terminal.println(commands.getName() + " - " + commands.getDescription());
        }
        while (true) {
            terminal.println("Введите комманду:");
            EditorContext.setProject(project);
            String command = terminal.readLine();
            if ("end".equalsIgnoreCase(command)) {
                EditorContext.cleanContext();
                return;
            } else {
                commandsExecutor.execute(command);
                EditorContext.cleanContext();
            }
        }
    }
}
