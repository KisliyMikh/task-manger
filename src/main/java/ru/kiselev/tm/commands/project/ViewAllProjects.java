package ru.kiselev.tm.commands.project;

import lombok.Data;
import ru.kiselev.tm.commands.CommandsInterface;
import ru.kiselev.tm.constants.Constants;
import ru.kiselev.tm.entity.Project;
import ru.kiselev.tm.repository.ProjectRepository;
import ru.kiselev.tm.service.Terminal;

import java.util.List;

@Data

public class ViewAllProjects implements CommandsInterface {
    private final String name = Constants.VIEW_ALL_PROJECTS;
    private final String description = "список всех проектов.";

    private final ProjectRepository projectRepository;
    private final Terminal terminal;

    @Override
    public void execute() {
        terminal.println("Все созданные проекты:");
        List<Project> projectList = projectRepository.getProjectList();
        if (!projectList.isEmpty()) {
            projectList.forEach(terminal::println);
        } else {
            terminal.println("Список проетов пуст.");
        }
    }
}
