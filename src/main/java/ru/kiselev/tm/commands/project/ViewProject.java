package ru.kiselev.tm.commands.project;

import lombok.Data;
import ru.kiselev.tm.commands.CommandsInterface;
import ru.kiselev.tm.constants.Constants;
import ru.kiselev.tm.entity.Project;
import ru.kiselev.tm.entity.Task;
import ru.kiselev.tm.repository.ProjectRepository;
import ru.kiselev.tm.repository.TaskRepository;
import ru.kiselev.tm.service.Terminal;

import java.io.IOException;
import java.util.List;

@Data

public class ViewProject implements CommandsInterface {
    private final String name = Constants.VIEW_PROJECT;
    private final String description = "просмотреть проект.";

    private final ProjectRepository projectRepository;
    private final TaskRepository taskRepository;
    private final Terminal terminal;

    @Override
    public void execute() throws IOException {
        terminal.println("Введите ID проекта:");
        Long idProject = Long.parseLong(terminal.readLine());
        Project project = projectRepository.getProject(idProject);
        terminal.println("Имя проекта: " + project.getNameProject());
        terminal.println("Описание проекта: " + project.getDescription());
        terminal.println("Дата начала проекта: " + project.getDataStart());
        terminal.println("Дата окончания проекта: " + project.getDataEnd());
        terminal.println("Задачи проекта:");
        List<Long> taskList = project.getTaskList();
        for (long taskId : taskList) {
            Task task = taskRepository.getTask(taskId);
            terminal.println(task);
        }
    }
}
