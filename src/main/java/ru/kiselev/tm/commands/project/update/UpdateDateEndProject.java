package ru.kiselev.tm.commands.project.update;

import lombok.Data;
import ru.kiselev.tm.commands.CommandsInterface;
import ru.kiselev.tm.constants.Constants;
import ru.kiselev.tm.entity.Project;
import ru.kiselev.tm.service.EditorContext;
import ru.kiselev.tm.service.Terminal;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;

@Data

public class UpdateDateEndProject implements CommandsInterface {
    private final String name = Constants.UPDATE_DATE_END;
    private final String description = "обновить дату окончания проекта.";

    private final Terminal terminal;

    @Override
    public void execute() throws IOException {
        terminal.println("Введите новую дату окончания проекта:");
        String dateEnd = terminal.readLine();
        Project project = EditorContext.getProject();
        if (dateEnd.isEmpty()) {
            project.setDataEnd(null);
        } else {
            try {
                LocalDate localDateProject = LocalDate.parse(dateEnd);
                project.setDataEnd(localDateProject);
            } catch (DateTimeParseException e) {
                terminal.println("Неверно введена дата! Повторите попытку.");
                execute();
            }
        }
    }
}
