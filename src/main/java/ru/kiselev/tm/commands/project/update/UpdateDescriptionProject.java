package ru.kiselev.tm.commands.project.update;

import lombok.Data;
import ru.kiselev.tm.commands.CommandsInterface;
import ru.kiselev.tm.constants.Constants;
import ru.kiselev.tm.entity.Project;
import ru.kiselev.tm.service.EditorContext;
import ru.kiselev.tm.service.Terminal;

import java.io.IOException;

@Data

public class UpdateDescriptionProject implements CommandsInterface {
    private final String name = Constants.UPDATE_DESCRIPTION;
    private final String description = "обновить описание проекта.";

    private final Terminal terminal;

    @Override
    public void execute() throws IOException {
        terminal.println("Введите новое описание проекта:");
        String descriptionProject = terminal.readLine();
        Project project = EditorContext.getProject();
        project.setDescription(descriptionProject);
    }
}
