package ru.kiselev.tm.commands.project.update;

import lombok.Data;
import ru.kiselev.tm.commands.CommandsInterface;
import ru.kiselev.tm.constants.Constants;
import ru.kiselev.tm.entity.Project;
import ru.kiselev.tm.service.EditorContext;
import ru.kiselev.tm.service.StringUtils;
import ru.kiselev.tm.service.Terminal;

import java.io.IOException;

@Data

public class UpdateNameProject implements CommandsInterface {
    private final String name = Constants.UPDATE_NAME;
    private final String description = "обновить имя проекта.";

    private final Terminal terminal;

    @Override
    public void execute() throws IOException {
        terminal.println("Введите новое имя проекта:");
        String name = terminal.readLine();
        Project project = EditorContext.getProject();
        boolean projectName = StringUtils.isEmpty(name);
        if (projectName == true) {
            throw new IllegalArgumentException("Не введено новое имя проекта! Повторите попытку.");
        } else {
            project.setNameProject(name);
        }
    }
}
