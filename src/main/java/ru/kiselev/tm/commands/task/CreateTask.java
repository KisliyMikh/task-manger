package ru.kiselev.tm.commands.task;

import lombok.Data;
import ru.kiselev.tm.commands.CommandsInterface;
import ru.kiselev.tm.constants.Constants;
import ru.kiselev.tm.entity.Project;
import ru.kiselev.tm.entity.Task;
import ru.kiselev.tm.repository.TaskRepository;
import ru.kiselev.tm.service.EditorContext;
import ru.kiselev.tm.service.StringUtils;
import ru.kiselev.tm.service.Terminal;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;


@Data

public class CreateTask implements CommandsInterface {
    private final String name = Constants.CREATE_TASK;
    private final String description = "создать задачу.";

    private final Terminal terminal;
    private final TaskRepository taskRepository;


    @Override
    public void execute() throws IOException {
        Task task = new Task();
        Project project = EditorContext.getProject();
        terminal.println("Введите название задачи:");
        String name = terminal.readLine();
        boolean taskName = StringUtils.isEmpty(name);
        if (taskName == true) {
            terminal.println("Не введено имя задачи! Повторите попытку.");
            return;
        } else {
            task.setNameTask(name);
        }
        terminal.println("Введите описание задачи:");
        String descriptionTask = terminal.readLine();
        task.setDescription(descriptionTask);
        terminal.println("Введите дату начала задачи в формате ГГГГ-ММ-ДД:");
        setDateTaskStart(task);
        terminal.println("Введите дату окончания задачи в формате ГГГГ-ММ-ДД:");
        setDateTaskEnd(task);
        task.setIdProject(project.getId());
        taskRepository.create(task);
        project.addTask(task.getId());
        terminal.println("Задача успешна создана! ID задачи: " + task.getId());
    }

    private void setDateTaskStart(Task task) throws IOException {
        String dateStart = terminal.readLine();
        if (dateStart.isEmpty()) {
            task.setDataStart(null);
        } else {
            try {
                LocalDate localDateTask = LocalDate.parse(dateStart);
                task.setDataStart(localDateTask);
            } catch (DateTimeParseException e) {
                terminal.println("Не вверно введена дата! Повторите попытку.");
                setDateTaskStart(task);
            }
        }
    }

    private void setDateTaskEnd(Task task) throws IOException {
        String dateEnd = terminal.readLine();
        if (dateEnd.isEmpty()) {
            task.setDataEnd(null);
        } else {
            try {
                LocalDate localDateTask = LocalDate.parse(dateEnd);
                task.setDataEnd(localDateTask);
            } catch (DateTimeParseException e) {
                terminal.println("Не вверно введена дата! Повторите попытку.");
                setDateTaskEnd(task);
            }
        }
    }
}
