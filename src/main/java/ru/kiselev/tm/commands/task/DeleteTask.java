package ru.kiselev.tm.commands.task;

import lombok.Data;
import ru.kiselev.tm.commands.CommandsInterface;
import ru.kiselev.tm.constants.Constants;
import ru.kiselev.tm.entity.Project;
import ru.kiselev.tm.entity.Task;
import ru.kiselev.tm.repository.ProjectRepository;
import ru.kiselev.tm.repository.TaskRepository;
import ru.kiselev.tm.service.Terminal;

import java.io.IOException;

@Data

public class DeleteTask implements CommandsInterface {
    private final String name = Constants.DELETE_TASK;
    private final String description = "удалить задачу.";

    private final Terminal terminal;
    private final TaskRepository taskRepository;
    private final ProjectRepository projectRepository;

    @Override
    public void execute() throws IOException {
        terminal.println("Введите ID задачи:");
        Long idTask = Long.parseLong(terminal.readLine());
        Task task = taskRepository.getTask(idTask);
        Project project = projectRepository.getProject(task.getIdProject());
        project.getTaskList().remove(idTask);
        taskRepository.remove(idTask);
        terminal.println("Задача успешно удалена!\n");
    }
}
