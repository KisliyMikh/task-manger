package ru.kiselev.tm.commands.task;

import lombok.Data;
import ru.kiselev.tm.commands.CommandsExecutor;
import ru.kiselev.tm.commands.CommandsHolder;
import ru.kiselev.tm.commands.CommandsInterface;
import ru.kiselev.tm.commands.other.End;
import ru.kiselev.tm.commands.other.Help;
import ru.kiselev.tm.commands.task.update.*;
import ru.kiselev.tm.constants.Constants;
import ru.kiselev.tm.entity.Task;
import ru.kiselev.tm.repository.ProjectRepository;
import ru.kiselev.tm.repository.TaskRepository;
import ru.kiselev.tm.service.EditorContext;
import ru.kiselev.tm.service.Terminal;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


@Data

public class EditTask implements CommandsInterface {
    private final String name = Constants.EDIT_TASK;
    private final String description = "редактировать задачу.";

    private final TaskRepository taskRepository;
    private final ProjectRepository projectRepository;
    private final Terminal terminal;
    private Map<String, CommandsInterface> taskCommands;
    private final CommandsHolder commandsHolder;
    private final CommandsExecutor commandsExecutor;

    public EditTask(TaskRepository taskRepository, ProjectRepository projectRepository, Terminal terminal) {
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
        this.terminal = terminal;
        this.taskCommands = new HashMap<>() {{
            put(Constants.UPDATE_NAME, new UpdateNameTask(terminal));
            put(Constants.UPDATE_DATE_START, new UpdateDateStartTask(terminal));
            put(Constants.UPDATE_DATE_END, new UpdateDateEndTask(terminal));
            put(Constants.UPDATE_DESCRIPTION, new UpdateDescriptionTask(terminal));
            put(Constants.MOVE_TASK_TO_PROJECT, new MoveTaskToProject(terminal, projectRepository));
            put(Constants.DELETE_TASK, new DeleteTask(terminal, taskRepository, projectRepository));
            put(Constants.HELP, new Help(this, terminal));
            put(Constants.END, new End());
        }};
        this.commandsHolder = new CommandsHolder(taskCommands);
        this.commandsExecutor = new CommandsExecutor(commandsHolder, terminal);
    }


    @Override
    public void execute() throws IOException {
        terminal.println("Введите ID задачи:");
        Long idTask = Long.parseLong(terminal.readLine());
        Task task = taskRepository.getTask(idTask);
        terminal.println("Введите одну из ниже предложенных команд:\n");
        for (CommandsInterface commands : taskCommands.values()) {
            terminal.println(commands.getName() + " - " + commands.getDescription());
        }
        while (true) {
            terminal.println("Введите команду:");
            EditorContext.setTask(task);
            String command = terminal.readLine();
            if ("end".equalsIgnoreCase(command)) {
                EditorContext.cleanContext();
                return;
            } else {
                commandsExecutor.execute(command);
                EditorContext.cleanContext();
            }
        }
    }
}
