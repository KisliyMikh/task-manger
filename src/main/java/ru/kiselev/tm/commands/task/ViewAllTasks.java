package ru.kiselev.tm.commands.task;

import lombok.Data;
import ru.kiselev.tm.commands.CommandsInterface;
import ru.kiselev.tm.constants.Constants;
import ru.kiselev.tm.entity.Task;
import ru.kiselev.tm.repository.TaskRepository;
import ru.kiselev.tm.service.Terminal;

import java.util.List;

@Data

public class ViewAllTasks implements CommandsInterface {
    private final String name = Constants.VIEW_ALL_TASKS;
    private final String description = "список всех задач.";

    private final TaskRepository taskRepository;
    private final Terminal terminal;

    @Override
    public void execute() {
        terminal.println("Все созданные задачи:");
        List<Task> taskList = taskRepository.getTaskList();
        taskList.forEach(terminal::println);
    }
}
