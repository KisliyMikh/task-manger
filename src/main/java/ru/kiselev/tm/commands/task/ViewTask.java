package ru.kiselev.tm.commands.task;

import lombok.Data;
import ru.kiselev.tm.commands.CommandsInterface;
import ru.kiselev.tm.constants.Constants;
import ru.kiselev.tm.entity.Task;
import ru.kiselev.tm.repository.TaskRepository;
import ru.kiselev.tm.service.Terminal;

import java.io.IOException;

@Data

public class ViewTask implements CommandsInterface {
    private final String name = Constants.VIEW_TASK;
    private final String description = "просмотреть задачу.";

    private final Terminal terminal;
    private final TaskRepository taskRepository;

    @Override
    public void execute() throws IOException {
        terminal.println("Введите ID задачи:");
        Long idTask = Long.parseLong(terminal.readLine());
        Task task = taskRepository.getTask(idTask);
        terminal.println("Имя задачи: " + task.getNameTask());
        terminal.println("Описание задачи: " + task.getDescription());
        terminal.println("Дата начала задачи: " + task.getDataStart());
        terminal.println("Дата окончания задачи: " + task.getDataEnd());
        terminal.println("ID проекта к которому прикреплена задача: " + task.getIdProject());
    }
}
