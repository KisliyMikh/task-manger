package ru.kiselev.tm.commands.task.update;

import lombok.Data;
import ru.kiselev.tm.commands.CommandsInterface;
import ru.kiselev.tm.constants.Constants;
import ru.kiselev.tm.entity.Project;
import ru.kiselev.tm.entity.Task;
import ru.kiselev.tm.repository.ProjectRepository;
import ru.kiselev.tm.service.EditorContext;
import ru.kiselev.tm.service.Terminal;

import java.io.IOException;

@Data
public class MoveTaskToProject implements CommandsInterface {
    private final String name = Constants.MOVE_TASK_TO_PROJECT;
    private final String description = "перместить задачу в другой проект.";

    private final Terminal terminal;
    private final ProjectRepository projectRepository;


    @Override
    public void execute() throws IOException {
        Task moveTask = EditorContext.getTask();
        Project fromProject = EditorContext.getProject();
        terminal.println("Введите ID проекта к которому хотите присоединить задачу:");
        Long idProject = Long.parseLong(terminal.readLine());
        Project toProject = projectRepository.getProject(idProject);
        //записываем переносимую задачу в список задач нового проекта
        toProject.addTask(moveTask.getId());
        //прикрепляем задачу к новому проекту
        moveTask.setIdProject(toProject.getId());
        //удаляем из старого проекта переносимую задачу
        fromProject.getTaskList().remove(moveTask.getId());
    }
}
