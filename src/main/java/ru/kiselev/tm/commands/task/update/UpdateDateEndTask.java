package ru.kiselev.tm.commands.task.update;

import lombok.Data;
import ru.kiselev.tm.commands.CommandsInterface;
import ru.kiselev.tm.constants.Constants;
import ru.kiselev.tm.entity.Task;
import ru.kiselev.tm.service.EditorContext;
import ru.kiselev.tm.service.Terminal;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;

@Data
public class UpdateDateEndTask implements CommandsInterface {
    private final String name = Constants.UPDATE_DATE_END;
    private final String description = "обновить дату окончания задачи";

    private final Terminal terminal;

    @Override
    public void execute() throws IOException {
        terminal.println("Введите новую дату окончание задачи в формате ГГГГ-ММ-ДД:");
        Task task = EditorContext.getTask();
        String dateEnd = terminal.readLine();
        if (dateEnd.isEmpty()) {
            task.setDataEnd(null);
        } else {
            try {
                LocalDate localDateTask = LocalDate.parse(dateEnd);
                task.setDataEnd(localDateTask);
            } catch (DateTimeParseException e) {
                terminal.println("Не вверно введена дата! Повторите попытку.");
                execute();
            }
        }
    }
}
