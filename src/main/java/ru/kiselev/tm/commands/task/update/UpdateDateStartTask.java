package ru.kiselev.tm.commands.task.update;

import lombok.Data;
import ru.kiselev.tm.commands.CommandsInterface;
import ru.kiselev.tm.constants.Constants;
import ru.kiselev.tm.entity.Task;
import ru.kiselev.tm.service.EditorContext;
import ru.kiselev.tm.service.Terminal;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;

@Data
public class UpdateDateStartTask implements CommandsInterface {
    private final String name = Constants.UPDATE_DATE_START;
    private final String description = "Обновить дату начала задачи.";

    private final Terminal terminal;

    @Override
    public void execute() throws IOException {
        terminal.println("Введите новую дату начала задачи в формате ГГГГ-ММ-ДД:");
        String dateStart = terminal.readLine();
        Task task = EditorContext.getTask();
        if (dateStart.isEmpty()) {
            task.setDataStart(null);
        } else {
            try {
                LocalDate localDateTask = LocalDate.parse(dateStart);
                task.setDataStart(localDateTask);
            } catch (DateTimeParseException e) {
                terminal.println("Не вверно введена дата! Повторите попытку.");
                execute();
            }
        }
    }
}
