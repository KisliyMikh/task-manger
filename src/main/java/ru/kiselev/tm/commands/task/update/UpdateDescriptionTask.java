package ru.kiselev.tm.commands.task.update;

import lombok.Data;
import ru.kiselev.tm.commands.CommandsInterface;
import ru.kiselev.tm.constants.Constants;
import ru.kiselev.tm.entity.Task;
import ru.kiselev.tm.service.EditorContext;
import ru.kiselev.tm.service.Terminal;

import java.io.IOException;

@Data
public class UpdateDescriptionTask implements CommandsInterface {
    private final String name = Constants.UPDATE_DESCRIPTION;
    private final String description = "Обновить опимание задачи.";

    private final Terminal terminal;

    @Override
    public void execute() throws IOException {
        terminal.println("Введите новое описание задачи:");
        String descriptionTask = terminal.readLine();
        Task task = EditorContext.getTask();
        task.setDescription(descriptionTask);
    }
}
