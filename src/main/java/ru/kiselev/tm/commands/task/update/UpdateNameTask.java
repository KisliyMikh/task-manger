package ru.kiselev.tm.commands.task.update;

import lombok.Data;
import ru.kiselev.tm.commands.CommandsInterface;
import ru.kiselev.tm.constants.Constants;
import ru.kiselev.tm.entity.Task;
import ru.kiselev.tm.service.EditorContext;
import ru.kiselev.tm.service.StringUtils;
import ru.kiselev.tm.service.Terminal;

import java.io.IOException;

@Data
public class UpdateNameTask implements CommandsInterface {
    private final String name = Constants.UPDATE_NAME;
    private final String description = "обновить имя задачи.";

    private final Terminal terminal;

    @Override
    public void execute() throws IOException {
        terminal.println("Введите новое имя задачи:");
        String name = terminal.readLine();
        Task task = EditorContext.getTask();
        boolean taskName = StringUtils.isEmpty(name);
        if (taskName == true) {
            throw new IllegalArgumentException("Не введено новое имя задачи! Повторите попытку.");
        } else {
            task.setNameTask(name);
        }
    }
}
