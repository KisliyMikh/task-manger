package ru.kiselev.tm.constants;

public class Constants {
    public static final String HELP = "help";
    public static final String CREATE_PROJECT = "create-project";
    public static final String VIEW_PROJECT = "view-project";
    public static final String VIEW_ALL_PROJECTS = "view-all-projects";
    public static final String EDIT_PROJECT = "edit-project";
    public static final String DELETE_PROJECT = "delete-project";
    public static final String CREATE_TASK = "create-task";
    public static final String VIEW_TASK = "view-task";
    public static final String VIEW_ALL_TASKS = "view-all-tasks";
    public static final String EDIT_TASK = "edit-task";
    public static final String DELETE_TASK = "delete-task";
    public static final String UPDATE_NAME = "update-name";
    public static final String UPDATE_DESCRIPTION = "update-description";
    public static final String UPDATE_DATE_START = "update-date-start";
    public static final String UPDATE_DATE_END = "update-date-end";
    public static final String MOVE_TASK_TO_PROJECT = "move-task-to-project";
    public static final String EXIT = "exit";
    public static final String END = "end";
}
