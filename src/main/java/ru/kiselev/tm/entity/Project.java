package ru.kiselev.tm.entity;

import lombok.Data;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Data
public class Project {
    private long id;
    private String nameProject;
    private String description;
    private LocalDate dataStart;
    private LocalDate dataEnd;
    private List<Long> taskList = new ArrayList<>();

    public String toString() {
        return "ID:" + getId() + " name:" + getNameProject() +
                " number of tasks:" + taskList.size();
    }

    public void addTask(Long id) {
        taskList.add(id);
    }
}


