package ru.kiselev.tm.entity;

import lombok.Data;

import java.time.LocalDate;

@Data
public class Task {
    private long id;
    private String nameTask;
    private String description;
    private LocalDate dataStart;
    private LocalDate dataEnd;
    private long idProject;

    public String toString() {
        return "ID task:" + getId() + " name:" + getNameTask() +
                " ID project:" + getIdProject();
    }
}
