package ru.kiselev.tm.exception;

public class CommandNotFoundException extends RuntimeException {
    public CommandNotFoundException(String commandName) {
        super("Не найдена команда по имени: " + commandName);
    }
}
