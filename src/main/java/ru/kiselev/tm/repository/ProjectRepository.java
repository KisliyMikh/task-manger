package ru.kiselev.tm.repository;

import ru.kiselev.tm.entity.Project;
import ru.kiselev.tm.service.IdCounter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProjectRepository implements Repository<Long, Project> {
    private final Map<Long, Project> projectMap;
    private final IdCounter countIdProject;

    public ProjectRepository() {
        this.projectMap = new HashMap<>();
        this.countIdProject = new IdCounter(1L);
    }

    public ProjectRepository(Map<Long, Project> projectMap, IdCounter countIdProject) {
        this.projectMap = projectMap;
        this.countIdProject = countIdProject;
    }

    @Override
    public Project create(Project project) {
        if (project == null) {
            throw new IllegalArgumentException("Попытка сохранить null проект");
        }
        project.setId(countIdProject.getNextId());
        projectMap.put(project.getId(), project);
        return project;
    }

    @Override
    public Project remove(Long id) {
        return projectMap.remove(id);
    }

    @Override
    public void clear() {
        projectMap.clear();
    }

    public List<Project> getProjectList() {
        return new ArrayList<>(projectMap.values());
    }

    public Project getProject(Long id) {
        Project getProject = projectMap.get(id);
        if (getProject == null) {
            throw new IllegalArgumentException("Проект с таким ID не существует!");
        }
        return projectMap.get(id);
    }
}
