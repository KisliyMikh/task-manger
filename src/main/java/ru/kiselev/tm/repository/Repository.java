package ru.kiselev.tm.repository;

public interface Repository<ID, ENTITY> {
    ENTITY create(ENTITY entity);

    ENTITY remove(ID id);

    void clear();

}


