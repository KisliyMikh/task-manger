package ru.kiselev.tm.repository;


import ru.kiselev.tm.entity.Task;
import ru.kiselev.tm.service.IdCounter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TaskRepository implements Repository<Long, Task> {
    private final Map<Long, Task> taskMap;
    private final IdCounter countIdTask;

    public TaskRepository() {
        this.taskMap = new HashMap<>();
        this.countIdTask = new IdCounter(1L);
    }

    public TaskRepository(Map<Long, Task> taskMap, IdCounter countIdTask) {
        this.taskMap = taskMap;
        this.countIdTask = countIdTask;
    }


    @Override
    public Task create(Task task) {
        if (task == null) {
            throw new IllegalArgumentException("Попытка сохранить null задачу.");
        }
        task.setId(countIdTask.getNextId());
        taskMap.put(task.getId(), task);
        return task;
    }

    @Override
    public Task remove(Long id) {
        return taskMap.remove(id);
    }

    @Override
    public void clear() {
        taskMap.clear();
    }


    public List<Task> getTaskList() {
        return new ArrayList<>(taskMap.values());
    }

    public Task getTask(Long id) {
        Task getTask = taskMap.get(id);
        if (getTask == null) {
            throw new IllegalArgumentException("Задачи с таким ID не существует!");
        }
        return getTask;
    }

    public void removeListTask(List<Long> taskList) {
        taskList.forEach(taskId -> taskMap.remove(taskId));
    }
}
