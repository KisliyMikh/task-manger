package ru.kiselev.tm.service;

import lombok.Getter;
import lombok.Setter;
import ru.kiselev.tm.entity.Project;
import ru.kiselev.tm.entity.Task;


public class EditorContext {

    @Setter
    @Getter
    private static Project project;
    @Setter
    @Getter
    private static Task task;

    public static void cleanContext() {
        project = null;
        task = null;
    }

}
