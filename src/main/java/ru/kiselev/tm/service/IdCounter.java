package ru.kiselev.tm.service;

public class IdCounter {
    private long counter;

    public IdCounter(long initialValue) {
        this.counter = initialValue;
    }

    public long getNextId() {
        return counter++;
    }
}
