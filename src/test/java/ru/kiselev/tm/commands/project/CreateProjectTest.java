package ru.kiselev.tm.commands.project;

import org.junit.jupiter.api.Test;
import org.mockito.InOrder;
import ru.kiselev.tm.entity.Project;
import ru.kiselev.tm.repository.ProjectRepository;
import ru.kiselev.tm.service.Terminal;

import java.io.IOException;
import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class CreateProjectTest {

    private final Terminal terminal = mock(Terminal.class);
    private final ProjectRepository repository = mock(ProjectRepository.class);
    private final CreateProject createProject = new CreateProject(repository, terminal);

    @Test
    void execute() throws IOException {
        Project expectedProject = new Project();
        expectedProject.setNameProject("project-name");
        expectedProject.setDescription("project-description");
        expectedProject.setDataStart(LocalDate.of(2000, 12, 12));
        expectedProject.setDataEnd(LocalDate.of(2012, 12, 12));
        when(terminal.readLine())
                .thenReturn("project-name", "project-description", "2000-12-12", "2012-12-12");

        createProject.execute();

        InOrder inOrder = inOrder(terminal, repository);
        inOrder.verify(terminal).println("Введите название проекта:");
        inOrder.verify(terminal).readLine();
        inOrder.verify(terminal).println("Введите описание проекта:");
        inOrder.verify(terminal).readLine();
        inOrder.verify(terminal).println("Введите дату начала проекта в формате ГГГГ-ММ-ДД:");
        inOrder.verify(terminal).readLine();
        inOrder.verify(terminal).println("Введите дату окончания проекта в формате ГГГГ-ММ-ДД:");
        inOrder.verify(terminal).readLine();
        inOrder.verify(repository).create(expectedProject);
        inOrder.verify(terminal).println("Проект успешно создан! ID проекта:0");
    }

    @Test
    void executeEmptyName() throws IOException {
        when(terminal.readLine()).thenReturn("");

        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> createProject.execute());

        assertEquals("Не введено название проекта! Повторите попытку.", exception.getMessage());
    }

    @Test
    void executeIllegalDateStart() throws IOException {
        Project project = new Project();
        when(terminal.readLine()).thenReturn("illegal-date-format", "2000-12-12");

        createProject.setDateProjectStart(project);

        InOrder inOrder = inOrder(terminal);
        inOrder.verify(terminal).readLine();
        inOrder.verify(terminal).println("Неверно введена дата! Повторите попытку.");
        inOrder.verify(terminal).readLine();
        assertEquals(LocalDate.of(2000, 12, 12), project.getDataStart());
    }

    @Test
    void executeEmptyDateStart() throws IOException {
        Project project = new Project();
        when(terminal.readLine()).thenReturn("");

        createProject.setDateProjectStart(project);

        InOrder inOrder = inOrder(terminal);
        inOrder.verify(terminal).readLine();
        assertNull(project.getDataStart());
    }

    @Test
    void executeIllegalDateEnd() throws IOException {
        Project project = new Project();
        when(terminal.readLine()).thenReturn("illegal-date-format", "2001-12-12");

        createProject.setDateProjectEnd(project);

        InOrder inOrder = inOrder(terminal);
        inOrder.verify(terminal).readLine();
        inOrder.verify(terminal).println("Неверно введена дата! Повторите попытку.");
        inOrder.verify(terminal).readLine();
        assertEquals(LocalDate.of(2001, 12, 12), project.getDataEnd());
    }

    @Test
    void executeEmptyDateEnd() throws IOException {
        Project project = new Project();
        when(terminal.readLine()).thenReturn("");

        createProject.setDateProjectEnd(project);

        InOrder inOrder = inOrder(terminal);
        inOrder.verify(terminal).readLine();
        assertNull(project.getDataEnd());
    }
}