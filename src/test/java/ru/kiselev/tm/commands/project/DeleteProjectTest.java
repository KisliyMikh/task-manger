package ru.kiselev.tm.commands.project;

import org.junit.jupiter.api.Test;
import org.mockito.InOrder;
import ru.kiselev.tm.entity.Project;
import ru.kiselev.tm.repository.ProjectRepository;
import ru.kiselev.tm.repository.TaskRepository;
import ru.kiselev.tm.service.Terminal;

import java.io.IOException;
import java.util.List;

import static org.mockito.Mockito.*;

class DeleteProjectTest {

    private final ProjectRepository projectRepository = mock(ProjectRepository.class);
    private final TaskRepository taskRepository = mock(TaskRepository.class);
    private final Terminal terminal = mock(Terminal.class);
    private final DeleteProject deleteProject = new DeleteProject(projectRepository, taskRepository, terminal);


    @Test
    void execute() throws IOException {
        Project project = new Project();
        List<Long> taskList = List.of(1L, 2L, 3L);
        project.setTaskList(taskList);
        when(terminal.readLine()).thenReturn("0");
        when(projectRepository.getProject(0L))
                .thenReturn(project);

        deleteProject.execute();

        InOrder inOrder = inOrder(terminal, projectRepository, taskRepository);
        inOrder.verify(terminal).println("Введите ID проекта:");
        inOrder.verify(terminal).readLine();
        inOrder.verify(taskRepository).removeListTask(taskList);
        inOrder.verify(projectRepository).remove(0L);
        inOrder.verify(terminal).println("Проект с задачами успешно удален!\n");
    }
}
