package ru.kiselev.tm.commands.project;

import org.junit.jupiter.api.Test;
import org.mockito.InOrder;
import org.mockito.Mockito;
import ru.kiselev.tm.entity.Project;
import ru.kiselev.tm.repository.ProjectRepository;
import ru.kiselev.tm.service.Terminal;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class ViewAllProjectsTest {

    private final ProjectRepository projectRepository = mock(ProjectRepository.class);
    private final Terminal terminal = mock(Terminal.class);
    private final ViewAllProjects viewAllProjects = new ViewAllProjects(projectRepository, terminal);

    @Test
    void execute() {
        Project project = new Project();
        project.setId(0L);
        project.setNameProject("project-name");
        List <Long> taskList = List.of(1L, 2L, 3L);
        project.setTaskList(taskList);
        List <Project> projectList = List.of(project);
        when(projectRepository.getProjectList()).thenReturn(projectList);

        viewAllProjects.execute();

        InOrder inOrder = Mockito.inOrder(terminal, projectRepository);
        inOrder.verify(terminal).println("Все созданные проекты:");
        inOrder.verify(projectRepository).getProjectList();
        inOrder.verify(terminal).println(project);

    }

    @Test
    void nullProjects(){
        when(projectRepository.getProjectList()).thenReturn(new ArrayList<>());

        viewAllProjects.execute();

        InOrder inOrder = Mockito.inOrder(terminal, projectRepository);
        inOrder.verify(terminal).println("Все созданные проекты:");
        inOrder.verify(projectRepository).getProjectList();
        inOrder.verify(terminal).println("Список проетов пуст.");
    }
}