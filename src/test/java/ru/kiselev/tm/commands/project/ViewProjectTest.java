package ru.kiselev.tm.commands.project;

import org.junit.jupiter.api.Test;
import org.mockito.InOrder;
import org.mockito.Mockito;
import ru.kiselev.tm.entity.Project;
import ru.kiselev.tm.entity.Task;
import ru.kiselev.tm.repository.ProjectRepository;
import ru.kiselev.tm.repository.TaskRepository;
import ru.kiselev.tm.service.Terminal;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class ViewProjectTest {

    private final ProjectRepository projectRepository = mock(ProjectRepository.class);
    private final TaskRepository taskRepository = mock(TaskRepository.class);
    private final Terminal terminal = mock(Terminal.class);
    private final ViewProject viewProject = new ViewProject(projectRepository, taskRepository, terminal);


    @Test
    void execute() throws IOException {
        Project project = new Project();
        project.setId(0L);
        project.setNameProject("project-name");
        project.setDescription("project-description");
        project.setDataStart(LocalDate.of(2020,1,1));
        project.setDataEnd(LocalDate.of(2020,2,2));
        Task task = new Task();
        task.setId(1L);
        task.setNameTask("task-name");
        task.setIdProject(0L);
        List <Long> taskList = List.of(1L);
        project.setTaskList(taskList);
        when(terminal.readLine()).thenReturn("0");
        when(projectRepository.getProject(0L)).thenReturn(project);
        when(taskRepository.getTask(1L)).thenReturn(task);

        viewProject.execute();

        InOrder inOrder = Mockito.inOrder(terminal, taskRepository, projectRepository);
        inOrder.verify(terminal).println("Введите ID проекта:");
        inOrder.verify(terminal).readLine();
        inOrder.verify(projectRepository).getProject(0L);
        inOrder.verify(terminal).println("Имя проекта: project-name");
        inOrder.verify(terminal).println("Описание проекта: project-description");
        inOrder.verify(terminal).println("Дата начала проекта: 2020-01-01");
        inOrder.verify(terminal).println("Дата окончания проекта: 2020-02-02");
        inOrder.verify(terminal).println("Задачи проекта:");
        inOrder.verify(terminal).println(task);
    }
}