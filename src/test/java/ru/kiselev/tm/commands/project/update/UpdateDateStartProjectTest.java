package ru.kiselev.tm.commands.project.update;

import org.junit.jupiter.api.Test;
import org.mockito.InOrder;
import org.mockito.Mockito;
import ru.kiselev.tm.entity.Project;
import ru.kiselev.tm.service.EditorContext;
import ru.kiselev.tm.service.Terminal;

import java.io.IOException;
import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class UpdateDateStartProjectTest {

    private final Terminal terminal = mock(Terminal.class);
    private final EditorContext context = mock(EditorContext.class);
    private final UpdateDateStartProject updateDateStartProject = new UpdateDateStartProject(terminal);

    @Test
    void execute() throws IOException {
        Project project = new Project();
        context.setProject(project);
        when(terminal.readLine()).thenReturn("", "2020-04-01");

        updateDateStartProject.execute();

        InOrder inOrder = Mockito.inOrder(terminal);
        inOrder.verify(terminal).println("Введите новую дату начала проекта:");
        inOrder.verify(terminal).readLine();
        assertNull(project.getDataStart());

        updateDateStartProject.execute();

        inOrder.verify(terminal).println("Введите новую дату начала проекта:");
        inOrder.verify(terminal).readLine();
        assertEquals(LocalDate.of(2020, 4, 1), project.getDataStart());
    }

    @Test
    void executeIllegalDateStartProject() throws IOException {
        Project project = new Project();
        EditorContext.setProject(project);
        when(terminal.readLine()).thenReturn("illegal-date", "2020-01-01");

        updateDateStartProject.execute();

        InOrder inOrder = Mockito.inOrder(terminal);
        inOrder.verify(terminal).println("Введите новую дату начала проекта:");
        inOrder.verify(terminal).readLine();
        inOrder.verify(terminal).println("Неверно введена дата! Повторите попытку.");
        inOrder.verify(terminal).println("Введите новую дату начала проекта:");
        inOrder.verify(terminal).readLine();
        assertEquals(LocalDate.of(2020, 1, 1), project.getDataStart());
    }
}
