package ru.kiselev.tm.commands.project.update;

import org.junit.jupiter.api.Test;
import org.mockito.InOrder;
import org.mockito.Mockito;
import ru.kiselev.tm.entity.Project;
import ru.kiselev.tm.service.EditorContext;
import ru.kiselev.tm.service.Terminal;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class UpdateDescriptionProjectTest {


    private final Terminal terminal = mock(Terminal.class);
    private final UpdateDescriptionProject updateDescriptionProject = new UpdateDescriptionProject(terminal);

    @Test
    void execute() throws IOException {
        Project project = new Project();
        EditorContext.setProject(project);
        when(terminal.readLine()).thenReturn("Qwe");

        updateDescriptionProject.execute();

        InOrder inOrder = Mockito.inOrder(terminal);
        inOrder.verify(terminal).println("Введите новое описание проекта:");
        inOrder.verify(terminal).readLine();
        assertNotNull(project.getDescription());
    }
}