package ru.kiselev.tm.commands.project.update;

import org.junit.jupiter.api.Test;
import org.mockito.InOrder;
import org.mockito.Mockito;
import ru.kiselev.tm.entity.Project;
import ru.kiselev.tm.service.EditorContext;
import ru.kiselev.tm.service.Terminal;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class UpdateNameProjectTest {

    private final Terminal terminal = mock(Terminal.class);
    private final UpdateNameProject updateNameProject = new UpdateNameProject(terminal);

    @Test
    void execute() throws IOException {
        Project project = new Project();
        EditorContext.setProject(project);
        when(terminal.readLine()).thenReturn("qwe");

        updateNameProject.execute();

        InOrder inOrder = Mockito.inOrder(terminal);
        inOrder.verify(terminal).println("Введите новое имя проекта:");
        inOrder.verify(terminal).readLine();
        assertEquals("qwe", project.getNameProject());
    }

    @Test
    void nullNameUpdate() {
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, ()
                -> updateNameProject.execute());

        assertEquals("Не введено новое имя проекта! Повторите попытку.", exception.getMessage());
    }
}