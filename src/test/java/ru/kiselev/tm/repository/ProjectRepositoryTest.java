package ru.kiselev.tm.repository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ru.kiselev.tm.entity.Project;
import ru.kiselev.tm.service.IdCounter;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class ProjectRepositoryTest {

    private long testInitialCounterValue = 9L;
    private IdCounter counter;
    private Map<Long, Project> projectMap;
    private ProjectRepository repository;

    @BeforeEach
    void setUp() {
        this.counter = new IdCounter(testInitialCounterValue);
        this.projectMap = new HashMap<>();
        this.repository = new ProjectRepository(projectMap, counter);
    }

    @Test
    void create() {
        Project newProject = new Project();

        Project savedProject = repository.create(newProject);

        assertEquals(newProject, savedProject);
        assertEquals(testInitialCounterValue, savedProject.getId());
        assertTrue(projectMap.containsKey(savedProject.getId()));
        assertTrue(projectMap.containsValue(savedProject));
    }

    @Test
    void createNull() {
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> repository.create(null));

        assertEquals("Попытка сохранить null проект", exception.getMessage());
    }

    @Test
    void remove() {
        Project newProject = new Project();
        newProject.setId(100);
        projectMap.put(newProject.getId(), newProject);

        repository.remove(newProject.getId());

        assertFalse(projectMap.containsKey(newProject.getId()));
    }

    @Test
    void clear() {
        Project newProject = new Project();
        newProject.setId(100);
        projectMap.put(newProject.getId(), newProject);

        repository.clear();

        assertEquals(projectMap.size(), 0);
    }

    @Test
    void getProjectList() {
        Project newProject = new Project();
        newProject.setId(100);
        projectMap.put(newProject.getId(), newProject);
        Project newProject2 = new Project();
        newProject2.setId(1000);
        projectMap.put(newProject2.getId(), newProject2);

        List<Project> projectList = repository.getProjectList();

        assertEquals(projectMap.size(), 2);
        assertTrue(projectList.containsAll(List.of(newProject, newProject2)));
    }

    @Test
    void getProject() {
        Project newProject = new Project();
        newProject.setId(100);
        projectMap.put(newProject.getId(), newProject);

        Project getProject = repository.getProject(newProject.getId());

        assertEquals(newProject, getProject);
    }

    @Test
    void nullGetProject() {
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> repository.getProject(null));

        assertEquals("Проект с таким ID не существует!", exception.getMessage());
    }
}