package ru.kiselev.tm.repository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ru.kiselev.tm.entity.Task;
import ru.kiselev.tm.service.IdCounter;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

public class TaskRepositoryTest {
    private Map<Long, Task> taskMap;
    private IdCounter idCounter;
    private long testInitialCounterValue = 9L;
    private TaskRepository repository;

    @BeforeEach
    void setUp() {
        this.idCounter = new IdCounter(testInitialCounterValue);
        this.taskMap = new HashMap<>();
        this.repository = new TaskRepository(taskMap, idCounter);
    }

    @Test
    void create() {
        Task task = new Task();

        Task savedTask = repository.create(task);

        assertEquals(task, savedTask);
        assertEquals(testInitialCounterValue, savedTask.getId());
        assertTrue(taskMap.containsKey(savedTask.getId()));
        assertTrue(taskMap.containsValue(savedTask));
    }

    @Test
    void createNull() {
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> repository.create(null));

        assertEquals("Попытка сохранить null задачу.", exception.getMessage());
    }

    @Test
    void remove() {
        Task newTask = new Task();
        newTask.setId(100L);
        taskMap.put(newTask.getId(), newTask);

        repository.remove(newTask.getId());

        assertNull(taskMap.get(newTask.getId()));
    }

    @Test
    void removeListTask() {
        Task newTask = new Task();
        newTask.setId(100L);
        Task newTask1 = new Task();
        newTask1.setId(101L);
        List<Long> taskId = List.of(newTask.getId(), newTask1.getId());
        taskMap.put(newTask.getId(), newTask);
        taskMap.put(newTask1.getId(), newTask1);

        repository.removeListTask(taskId);

        assertNull(taskMap.get(newTask.getId()));
        assertNull(taskMap.get(newTask1.getId()));
    }

    @Test
    void clear() {
        Task newTask = new Task();
        newTask.setId(100L);
        taskMap.put(newTask.getId(), newTask);

        repository.clear();

        assertEquals(taskMap.size(), 0);
        assertFalse(taskMap.containsKey(newTask.getId()));
    }

    @Test
    void getTaskList() {
        Task newTask = new Task();
        newTask.setId(100);
        taskMap.put(newTask.getId(), newTask);
        Task newTask2 = new Task();
        newTask2.setId(1000);
        taskMap.put(newTask2.getId(), newTask2);

        List<Task> taskList = repository.getTaskList();

        assertEquals(taskList.size(), 2);
        assertTrue(taskList.containsAll(List.of(newTask, newTask2)));
    }

    @Test
    void getTask() {
        Task newTask = new Task();
        newTask.setId(100);
        taskMap.put(newTask.getId(), newTask);

        Task getTask = repository.getTask(newTask.getId());

        assertEquals(getTask, newTask);
    }

    @Test
    void nullGetTask() {
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> repository.getTask(null));

        assertEquals("Задачи с таким ID не существует!", exception.getMessage());
    }
}
